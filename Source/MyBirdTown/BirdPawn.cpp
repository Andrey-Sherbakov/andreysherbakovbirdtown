// Fill out your copyright notice in the Description page of Project Settings.


#include "BirdPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Bird.h"
#include "Components/InputComponent.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ABirdPawn::ABirdPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void ABirdPawn::BeginPlay()
{
	Super::BeginPlay();
	CreateBirdActor();
}

// Called every frame
void ABirdPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABirdPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &ABirdPawn::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &ABirdPawn::HandlePlayerHorizontalInput);
}

void ABirdPawn::CreateBirdActor()
{
	BirdActor = GetWorld()->SpawnActor<ABird>(BirdActorClass, FTransform());
}

void ABirdPawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(BirdActor))
	{
		if (value > 0)
		{
			//BirdActor->AddActorWorldOffset(FVector(0, 0, 100));
		}
	}
}

void ABirdPawn::HandlePlayerHorizontalInput(float value)
{

}


